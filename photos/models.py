# coding: utf-8

from django.conf import settings
from django.db import models
from django.db.models import Q

class Photo(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    image_file = models.ImageField(upload_to='%Y/%m/%d/')
    is_active = models.BooleanField(default=True)
    description = models.TextField(null=True)
    tags = models.ManyToManyField('Tag')
    # likes = models.ManyToManyField(
    #     settings.AUTH_USER_MODEL, related_name='like_user_set')
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=True)

    class Meta:
        ordering = ['-updated_at', '-id']
        get_latest_by = 'updated_at'

    def __unicode__(self):
        return u"{0} - '{1}'".format(self.id, self.user)

    def is_liked(self, user):
        if isinstance(user, basestring):
            _q = Q(user_username=user)
        elif isinstance(user, self.user.__class__):
            _q = Q(user=user)
        else:
            return False

        return self.like_set.filter(_q & Q(is_active=True)).exists()

class Like(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    photo = models.ForeignKey(Photo)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=True)

    def __unicode__(self):
        return u"{0} 'photo : {1}'".format(self.id, self.photo.id)

class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    photo = models.ForeignKey(Photo)
    content = models.TextField(null=False, max_length=500)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=True)

    def __unicode__(self):
        return u"{0} 'photo : {1}'".format(self.id, self.photo.id)

class Tag(models.Model):
    name = models.CharField(max_length=80)

    def __unicode__(self):
        return u"{0}".format(self.name)
