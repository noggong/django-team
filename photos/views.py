# coding: utf-8

import base64

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.files.uploadedfile import SimpleUploadedFile

from photos.models import Photo, Comment, Tag, Like
from photos.forms import PhotoModelForm, CommentModelForm

def toppage(request):
    return HttpResponse('hello')

@login_required
def like_photo(request, photo_id):
    photo = get_object_or_404(Photo, pk=photo_id)

    like, _created = Like.objects.get_or_create(
        user=request.user, photo=photo,
        defaults={
            'user':request.user,
            'photo':photo,
        })

    if not _created:
        like.is_active = not like.is_active
        like.save()

    return HttpResponseRedirect(
        reverse('single_photo', kwargs={'photo_id':photo.id})
    )


def single_photo(request, photo_id):
    if int(photo_id) == 0:
        return HttpResponseRedirect(reverse('photo_toppage'))

    photo = get_object_or_404(Photo, pk=photo_id)

    if request.user.is_authenticated():
        is_liked = photo.is_liked(request.user)
    else:
        is_liked = False

    if request.method == "GET":
        comment_form = CommentModelForm()
    elif request.method == "POST":
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('login_url'))

        comment_form = CommentModelForm(request.POST)
        if comment_form.is_valid():
            _comment = comment_form.save(commit=False)
            _comment.user = request.user
            _comment.photo = photo
            _comment.save()

            return HttpResponseRedirect(request.path)

    return render(
        request,
        'single_photo.html',
        {
            'photo': photo,
            'comment_form': comment_form,
            'is_liked': is_liked,
        }
    )


def get_base64_image(data):
    if data.count(';base64,') == 0:
        return None

    _format, _content = data.split(';base64,')
    return base64.b64decode(_content)


@login_required
def edit_photo(request, photo_id=None):
    if request.method == 'GET':
        form = PhotoModelForm()
    elif request.method == 'POST':
        if request.POST.get('filtered_image_file'):
            _file = get_base64_image(
                request.POST.get('filtered_image_file', '')
            )
            _filedata = {
                'image_file': SimpleUploadedFile(
                    request.FILES['image_file'].name.split('/')[-1], _file
                )
            }
        else:
            _filedata = request.FILES

        form = PhotoModelForm(request.POST, _filedata)

        if form.is_valid():
            _photo = form.save(commit=False)
            _photo.user = request.user
            _photo.save()

            for _tagname in request.POST.get('tags', '').split(','):
                _tagname = _tagname.strip()
                _tag, _created = Tag.objects.get_or_create(
                    name=_tagname, defaults={'name':_tagname}
                )
                _photo.tags.add(_tag)

            return HttpResponseRedirect('/photos/{0}/'.format(_photo.id))

    return render(request, 'edit_photo.html', {
            'form': form,
        })

