from django.contrib import admin

from photos.models import Photo

class PhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'is_active', 'created_at', 'updated_at',)
    ordering = ('id', 'updated_at',)

admin.site.register(Photo, PhotoAdmin)
