"""
Django settings for pystagram project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0pe5im+c^a8!tq=2@(j29oswq!0fg$68*mc7)-ddyx7$tqs%bt'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
ALLOWED_HOSTS = ['localhost']


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'photos',
    'djangobower',
    'pipeline',
    'profiles',
    'social.apps.django_app.default',
    'suit',
    'django.contrib.admin',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pystagram.pystagram_middlewares.PystagramMiddleware',
    # 'pipeline.middleware.MinifyHTMLMiddleware',
)

ROOT_URLCONF = 'pystagram.urls'

WSGI_APPLICATION = 'pystagram.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'NAME': 'Pystagram',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'python',
        'PASSWORD': '1234',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/


TEMPLATE_DIRS = (
	os.path.join(BASE_DIR, 'templates'),
    # '/Users/junho/Documents/python-project/2.www/1.pystagram/pystagram/templates',
)

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'
BOWER_COMPONENTS_ROOT = os.path.join(BASE_DIR, 'components')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
    'djangobower.finders.BowerFinder',
)

BOWER_INSTALLED_APPS = (
    'jquery',
    'bootstrap-sass-official',
    'foundation',
)

PIPELINE_COMPILERS = (
    # 'pipeline.compilers.sass.SASSCompiler',
    'pipeline_compass.compiler.CompassCompiler',
)

# CSS Files.
PIPELINE_CSS = {
    # Project libraries.
    'vendor': {
        'source_filenames': (
            'bootstrap/dist/css/bootstrap.min.css',
            'bootstrap/dist/css/bootstrap-theme.min.css',
        ),
        # Compress passed libraries and have
        # the output in`static_files/css/libs.min.css`.
        'output_filename': 'css/libs.min.css',
    },
    'custom': {
        'source_filenames': (
            'css/main-sass.css',
        ),
        # Compress passed libraries and have
        # the output in`static_files/css/libs.min.css`.
        # 'output_filename': 'css/global/.min.css',
    }
    # ...
}
# JavaScript files.
PIPELINE_JS = {
    # Project JavaScript libraries.
    'vendor': {
        'source_filenames': (
            # 'jquery/jquery.min.map',
            'jquery/dist/jquery.min.js',
            'bootstrap/dist/js/bootstrap.min.js',
        ),
        # Compress all passed files into `static_files/js/libs.min.js`.
        'output_filename': 'js/libs.min.js',
    }
    # ...
}
# PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
# PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
# PIPELINE_ENABLED = True

LOGIN_REDIRECT_URL = '/login/'
LOGIN_URL = '/login/'
# LOGOUT_URL = ''

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

##python manage.py collectstatic
STATICFILES_DIRS = (
    (os.path.join(BASE_DIR, 'components/')),
)

STATIC_ROOT = os.path.join(BASE_DIR, 'asset/')
STATIC_URL = '/asset/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'static/')
MEDIA_URL = '/img/'

AUTHENTICTION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_URL_NAMESPACE = 'social'

SOCIAL_AUTH_FACEBOOK_KEY = '819074928150626'
SOCIAL_AUTH_FACEBOOK_SECRET = '259f9208d04e59c8aa7b0a2d82a3259c'

SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'

#Admin suit template
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)