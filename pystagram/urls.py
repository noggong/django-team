from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns
from django.conf import settings

from photos import urls as photo_urls
from profiles import urls as profile_urls

from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^$', 'profiles.views.timeline', name='timeline'),
    url(r'^photos/', include(photo_urls)),
    url(r'^profiles/', include(profile_urls)),

    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name':'login.html'}, name="login_url"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':'/login/'}, name="logout_url"),
    url(r'^signup/$', 'profiles.views.signup'),
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


handler404 = 'profiles.views.my_handler404'