# coding: utf-8

from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q

from profiles.forms import ProfileForm
from profiles.models import Profile, FollowFK, FollowM2M

from photos.models import Photo, Like
from pystagram.pystagram_exceptions import HelloWorldError


def get_page(page):
    try:
        page = int(page)
        if page < 1:
            page = 1
    except:
        page = 1

    return page

def get_timeline_photos(user, page=1):
    _per_page = 10
    page = get_page(page)

    try:
        _following = user.followM2M_user.follow_users.all()
    except FollowM2M.DoesNotExist:
        _following = []

    _photos = Photo.objects.filter(
        Q(user__in=_following) | Q(user=user)
    ).order_by('-updated_at')[
        (page - 1) * _per_page:page * _per_page
    ]
    _count = len(_photos)

    _prev = None
    for _i, _photo in enumerate(_photos, 1):
        if _count > 1 and not _prev:
            _prev = _photo
            continue
        elif _count == 1:
            _prev = _photo

        yield _prev

        if _count > 1:
            _q = Q(user__in=_following) & Q(is_active=True)
            _q = _q & Q(updated_at__lte=_prev.updated_at)
            _q = _q & Q(updated_at__gt=_photo.updated_at)

            _targets = Photo.objects.filter(
                id__in=Like.objects.values_list('photo_id').filter(_q)
            ).distinct()

            for _target in _targets:
                yield _target

        if _count > 1 and _i == _count:
            yield _photo

        _prev = _photo

#@login_required
def timeline(request):
    #raise HelloWorldError
    if request.user.is_authenticated() :
        photos = get_timeline_photos(request.user, request.GET.get('page', 1))

        return render(request, 'photos/timeline.html', {
            'photos': photos
            })
    else :
        res = []
        if request.first_photo :
            res.append('<p>피스타그램의 첫 사진</p>')
            res.append('<p><img src="{0}" /> </p>'.format(
                request.first_photo.image_file.url
            ))
        else :
            res.append('<p>서비스 개시!</p>')
        return HttpResponse(''.join(res))


def profile_page(request, username):
    UserModel = get_user_model()
    user = get_object_or_404(UserModel, username=username)

    paginator = Paginator(user.photo_set.all(), 5)
    current_page = paginator.page(request.GET.get('page', 1))

    followers = FollowM2M.objects.filter(follow_users__in=[user,])

    if followers.filter(user=request.user).exists():
        is_following = True
    else:
        is_following = False

    return render(request, 'profile_page.html', {
        'user': user,
        'photos': current_page,
        'is_following': is_following,
        'followers': followers,
        })


def profile_page_fk(request, username):
    UserModel = get_user_model()
    user = get_object_or_404(UserModel, username=username)

    paginator = Paginator(user.photo_set.all(), 5)
    current_page = paginator.page(request.GET.get('page', 1))

    followers = FollowFK.objects.filter(follow_user=user)

    if followers.filter(user=request.user).exists():
        is_following = True
    else:
        is_following = False

    return render(request, 'profile_page.html', {
        'user': user,
        'photos': current_page,
        'is_following': is_following,
        'followers': followers,
        })

@login_required
def follow_user(request, username):
    UserModel = get_user_model()
    user = get_object_or_404(UserModel, username=username)

    if request.user == user:
        return HttpResponseRedirect(
            reverse('profile_page', kwargs={'username':username})
        )

    _follow, _created = FollowM2M.objects.get_or_create(
        user=request.user, defaults={'user': request.user}
    )

    if FollowM2M.objects.filter(
        user=request.user, follow_users__in=[user,]).exists():
        _follow.follow_users.remove(user)
    else:
        _follow.follow_users.add(user)

    return HttpResponseRedirect(
        reverse('profile_page', kwargs={'username':username})
    )

@login_required
def follow_user_fk(request, username):
    UserModel = get_user_model()
    user = get_object_or_404(UserModel, username=username)

    if request.user == user:
        return HttpResponseRedirect(
            reverse('profile_page', kwargs={'username':username})
        )

    _follow, _created = FollowFK.objects.get_or_create(
        user=request.user,
        follow_user=user,
        defaults={
            'user': request.user,
            'follow_user': user,
        }
    )

    if not _created:
        _follow.delete()

    return HttpResponseRedirect(
        reverse('profile_page', kwargs={'username':username})
    )

@login_required
def change_profile(request):
    if hasattr(request.user, 'profile'):
        _user_profile = request.user.profile.__dict__
    else:
        _user_profile = {}

    if request.method == 'GET':
        profile_form = ProfileForm(initial=_user_profile)
    elif request.method == 'POST':
        profile_form = ProfileForm(request.POST)

        if profile_form.is_valid():
            _profile = profile_form.save(commit=False)

            if hasattr(request.user, 'profile'):
                _profile.pk = request.user.profile.pk

            _profile.user = request.user
            _profile.save()

            return HttpResponseRedirect(request.path)

    return render(request, 'change_profile.html', {
        'profile_form': profile_form,
        })

def signup(request):
    if request.method == 'GET':
        user_form = UserCreationForm()
        profile_form = ProfileForm()
    elif request.method == 'POST':
        user_form = UserCreationForm(request.POST)
        profile_form = ProfileForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            _user = user_form.save()
            _user.set_password(user_form.cleaned_data['password1'])
            _user.save()

            _profile = profile_form.save(commit=False)
            _profile.user = _user
            _profile.save()

            return HttpResponseRedirect('/photos/create/')

    return render(request, 'profiles/signup.html', {
        'user_form': user_form,
        'profile_form': profile_form,
        })

def my_handler404(request):
    return render(request, 'error_404.html')