from django.conf.urls import patterns, url

urlpatterns = patterns('profiles.views',
    url(r'^settings/$', 'change_profile', name='change_profile'),
    url(r'^(?P<username>[\w.@+-]+)/$', 'profile_page', name='profile_page'),
    url(r'^(?P<username>[\w.@+-]+)/follow/$', 'follow_user', name='follow_user'),
)
