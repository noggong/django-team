# coding: utf-8

from django import template

register = template.Library()

@register.filter(name='is_liked')
def is_liked(obj, user):
    return obj.is_liked(user)

#register.filter("is_liked", is_liked)
