# coding: utf-8

from django.db import models
from django.conf import settings

class Profile(models.Model):
    GENDER_CHOICES = (
        ('F', 'Female',),
        ('M', 'Male',),
    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    gender = models.CharField(
        max_length=1, choices=GENDER_CHOICES)

    def __str__(self):
        return self.gender

class FollowFK(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='followFK_user_set'
    )
    follow_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='followFK_follow_set'
    )

    @property
    def followers(self):
        for follow in self.__class__.objects.filter(user=self.user):
            yield follow.follow_user

class FollowM2M(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name='followM2M_user'
    )
    follow_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='followM2M_follow_set'
    )
